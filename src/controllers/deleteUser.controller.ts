import { Request, Response } from "express";
import DeleteUserService from "../services/user/deleteUserService";

class DeleteUserController {
  async handle(req: Request, res: Response) {
    const { uuid } = req.params;
    const deleteUser = new DeleteUserService();

    await deleteUser.execute({
      uuid,
    });
    return res.status(200).json({
      message: "User deleted with success",
    });
  }
}

export default DeleteUserController;
