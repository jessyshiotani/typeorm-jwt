import { Request, Response } from "express";
import AuthService from "../services/session/authUserService";

class LoginUserController {
  async handle(req: Request, res: Response) {
    const { email, password } = req.body;

    const authUser = new AuthService();

    const authResponse = await authUser.execute({
      email,
      password,
    });

    return res.json(authResponse);
  }
}

export default LoginUserController;
