import { Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import UserRepository from "../repositories/UserRepository";
import { classToClass } from "class-transformer";

class ListAllUsersController {
  async handle(req: Request, res: Response) {
    const userRepository = getCustomRepository(UserRepository);

    const allUsers = await userRepository.find();

    return res.json(classToClass(allUsers));
  }
}

export default ListAllUsersController;
