import { Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import UserRepository from "../repositories/UserRepository";
import { classToClass } from "class-transformer";

class ListUserController {
  async handle(req: Request, res: Response) {
    const userId = req.user.id;

    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findOne({
      where: {
        id: userId,
      },
    });

    return res.json(classToClass(user));
  }
}

export default ListUserController;
