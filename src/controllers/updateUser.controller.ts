import { Request, Response } from "express";
import UpdateUserService from "../services/user/updateUserService";

class UpdateUserController {
  async handle(req: Request, res: Response) {
    const { uuid } = req.params;
    const data = req.body;

    const updatedUserService = new UpdateUserService();

    const user = await updatedUserService.execute({
      uuid,
      data,
    });

    return res.json(user);
  }
}

export default UpdateUserController;
