import { Request, Response } from "express";
import CreateUserService from "../services/user/createUserService";

class CreateUserController {
  async handle(req: Request, res: Response) {
    const { name, email, password, isAdm } = req.body;

    const createUser = new CreateUserService();

    const user = await createUser.execute({
      name,
      email,
      password,
      isAdm,
    });

    return res.status(201).json(user);
  }
}

export default CreateUserController;
