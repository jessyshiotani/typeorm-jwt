import { Request, Response, NextFunction } from "express";
import AppError from "../errors/AppError";
import UserRepository from "../repositories/UserRepository";
import { getCustomRepository } from "typeorm";
import User from "../entities/User";
import { verify } from "jsonwebtoken";

import { AuthenticationProps } from "./authentication";

export default async function VerifyOwnsAccount(
  req: AuthenticationProps,
  res: Response,
  next: NextFunction
) {
  const userInfo = req.user;
  const { uuid } = req.params;

  const userRepository = getCustomRepository(UserRepository);
  console.log(uuid);

  const user = await userRepository.findOne({
    where: {
      id: uuid,
    },
  });

  if (userInfo.id !== uuid && userInfo.isAdm === false) {
    throw new AppError("Missing admin permissions", 401);
  }

  return next();
}
