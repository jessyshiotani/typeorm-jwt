import { Response, NextFunction } from "express";
import AppError from "../errors/AppError";

import { AuthenticationProps } from "./authentication";

export default async function VerifyIsAdm(
  req: AuthenticationProps,
  res: Response,
  next: NextFunction
) {
  const userInfo = req.user;

  if (userInfo.isAdm === false) {
    throw new AppError("Missing admin permissions", 401);
  }

  return next();
}
