import { Request, Response, NextFunction } from "express";
import auth from "../config/auth";
import { verify } from "jsonwebtoken";
import AppError from "../errors/AppError";

interface Token {
  iat: number;
  exp: number;
  id: string;
  isAdm: boolean;
}

export interface AuthenticationProps extends Request {
  user: any;
}

export default function Authentication(
  req: AuthenticationProps,
  res: Response,
  next: NextFunction
): void {
  const headerAuth = req.headers.authorization;

  if (!headerAuth) {
    throw new AppError("Missing authorization headers", 401);
  }

  try {
    const [, token] = headerAuth.split(" ");
    const { secret } = auth.jwt;

    const decoded = verify(token, secret);
    const { id, isAdm } = decoded as Token;

    req.user = {
      id: id,
      isAdm: isAdm,
    };

    return next();
  } catch (error) {
    throw new AppError("JWT Expired or sended incorrectly");
  }
}
