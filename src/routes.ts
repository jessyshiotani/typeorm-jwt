import { Router } from "express";
import Authentication from "./middlewares/authentication";
import CreateUserController from "./controllers/createUser.controller";
import LoginUserController from "./controllers/loginUser.controller";
import UpdateUserController from "./controllers/updateUser.controller";
import DeleteUserController from "./controllers/deleteUser.controller";
import ListAllUsersController from "./controllers/listAllUsers.controller";
import ListUserController from "./controllers/listUser.controller";
import VerifyIsAdm from "./middlewares/verifyIsAdm";
import VerifyOwnsAccount from "./middlewares/verifyOwnsAccount";

const routes = Router();

const createUserController = new CreateUserController();
const loginUserController = new LoginUserController();
const listAllUsersController = new ListAllUsersController();
const listUserController = new ListUserController();
const updateUserController = new UpdateUserController();
const deleteUserController = new DeleteUserController();

routes.post("/users", createUserController.handle);
routes.post("/login", loginUserController.handle);
routes.get(
  "/users",
  Authentication,
  VerifyIsAdm,
  listAllUsersController.handle
);
routes.get("/users/profile", Authentication, listUserController.handle);
routes.patch(
  "/users/:uuid",
  Authentication,
  VerifyOwnsAccount,
  updateUserController.handle
);
routes.delete(
  "/users/:uuid",
  Authentication,
  VerifyOwnsAccount,
  deleteUserController.handle
);

export default routes;
