import AppError from "../../errors/AppError";
import UserRepository from "../../repositories/UserRepository";
import auth from "../../config/auth";
import { getCustomRepository } from "typeorm";
import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";

interface Request {
  email: string;
  password: string;
}

interface Response {
  token: string;
}

export default class AuthService {
  public async execute({ email, password }: Request): Promise<Response> {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findByEmail(email);
    if (!user) {
      throw new AppError("Wrong email/password", 401);
    }

    const comparePassword = await compare(password, user.password);
    if (!comparePassword) {
      throw new AppError("Wrong email/password", 401);
    }

    const { isAdm, id } = user;

    const { expiresIn, secret } = auth.jwt;
    const token = sign({ isAdm, id }, secret, {
      expiresIn,
    });

    return {
      token,
    };
  }
}
