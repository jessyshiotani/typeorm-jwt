import { getCustomRepository } from "typeorm";
import User from "../../entities/User";
import UserRepository from "../../repositories/UserRepository";
import AppError from "../../errors/AppError";

interface Request {
  uuid: string;
  data: {};
}

class UpdateUserService {
  public async execute({ uuid, data }: Request): Promise<User> {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findOne({
      where: {
        id: uuid,
      },
    });

    if (!user) {
      throw new AppError("User not found");
    }

    if ("isAdm" in data) {
      throw new AppError("Cannot change isAdm field", 401);
    }

    await userRepository.update(uuid, data);

    return user;
  }
}

export default UpdateUserService;
