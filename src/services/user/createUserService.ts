import { getCustomRepository } from "typeorm";
import UserRepository from "../../repositories/UserRepository";
import User from "../../entities/User";
import { hash } from "bcryptjs";
import AppError from "../../errors/AppError";

interface Request {
  name: string;
  email: string;
  password: string;
  isAdm: boolean;
}

class CreateUserService {
  public async execute({
    email,
    name,
    password,
    isAdm,
  }: Request): Promise<User> {
    const userRepository = getCustomRepository(UserRepository);

    const userExists = await userRepository.findByEmail(email);

    if (userExists) {
      throw new AppError("E-mail already registered", 401);
    }

    const hashedPassword = await hash(password, 8);

    const user = userRepository.create({
      name,
      email,
      password: hashedPassword,
      isAdm,
    });

    await userRepository.save(user);

    return user;
  }
}

export default CreateUserService;
