import { DeleteResult, getCustomRepository } from "typeorm";
import AppError from "../../errors/AppError";
import UserRepository from "../../repositories/UserRepository";

interface Request {
  uuid: string;
}

export default class DeleteUserService {
  public async execute({ uuid }: Request): Promise<DeleteResult> {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findOne({
      where: {
        id: uuid,
      },
    });

    if (!user) {
      throw new AppError("User not found");
    }

    return await userRepository.delete(uuid);
  }
}
